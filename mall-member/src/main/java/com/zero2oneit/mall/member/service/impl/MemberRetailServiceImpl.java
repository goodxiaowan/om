package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.mapper.MemberRetailMapper;
import com.zero2oneit.mall.member.service.MemberRetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-12
 */
@Service
public class MemberRetailServiceImpl extends ServiceImpl<MemberRetailMapper, MemberRetail> implements MemberRetailService {

    @Autowired
    private MemberRetailMapper retailMapper;

    @Override
    public BoostrapDataGrid pageList(MemberRetailQueryObject qo) {
        int total = retailMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : retailMapper.selectAll(qo));
    }
}