package com.zero2oneit.mall.common.bean.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@Data
@TableName("prize_info")
public class PrizeInfo implements Serializable {

	private static final long serialVersionUID = 1L;

    	/**
	 * id
	 */
		@TableId
		private Long id;
		/**
	 * 奖品名称
	 */
		private String prizeName;
		/**
	 * 奖品价格
	 */
		private Integer prizePrice;
		/**
	 * 奖品图片
	 */
		private String logo;
		/**
	 * 奖品数量
	 */
		private Integer amount;
		/**
	 * 奖品等级
	 */
		private Integer prizeLevel;
	
}
