package com.zero2oneit.mall.common.query.goods;

import com.zero2oneit.mall.common.utils.query.QueryObject;
import lombok.Data;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-06-02
 */
@Data
public class NewcomersRuleQueryObject extends QueryObject {

    private String ruleName;

}
